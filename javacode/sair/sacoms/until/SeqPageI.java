package sair.sacoms.until;
/**
 * 排序类型需要被继承的接口
 * 
 * @author _Sair
 **/
public interface SeqPageI {
	/**
	 * 返回定义好的排序id，将依照id排序
	 * 
	 * @return Integer 类型
	 **/
	long getSeqID();
}
