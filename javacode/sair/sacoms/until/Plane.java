package sair.sacoms.until;

/**
 * 空间接口，封装一个长和宽
 **/
public interface Plane {
	/**
	 * 获取空间宽度
	 * 
	 * @return Integer
	 **/
	int getWidth();

	/**
	 * 获取空间高度
	 * 
	 * @return Integer
	 **/
	int getHeight();
}
