package sair.sacoms.until;

/**
 * 点位接口，封装一个含X和Y的对象
 **/
public interface Point {
	/**
	 * 获取X点
	 * 
	 * @return Integer
	 **/
	int getX();

	/**
	 * 获取Y点
	 * 
	 * @return Integer
	 **/
	int getY();
}
