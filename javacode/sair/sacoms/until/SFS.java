package sair.sacoms.until;

import sair.sacoms.utfcode.SFS_UFTCODE;

public class SFS {

	public static final char splits = 'G', zero = '0', low = '-', lowCh = '��', pointCh = '��', point = '.';
	public static final char[] VM = { splits, 'ʮ', '��', 'ǧ', '��', '��', '��', '��', '��', '��', '�', '��', '��', '��', '��', '��',
			'��', '��', '��', '��' };
	public static final char[] BigChMath = new char[] { '��', 'Ҽ', '�E', '��', '��', '��', '�', '��', '��', '��' };
	public static final char[] SmallChMath = new char[] { '��', 'һ', '��', '��', '��', '��', '��', '��', '��', '��' };

	@Deprecated
	public static final char[] miarry = SFS_UFTCODE.miarry;
}